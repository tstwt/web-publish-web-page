/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-28 14:27:29
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-28 14:27:29
 */
export default (min, max) => (Math.random() * (max - min)) + min;