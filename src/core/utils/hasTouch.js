/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-28 16:10:42
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-28 16:10:42
 */
export default () => 'ontouchstart' in document.documentElement ||
  navigator.maxTouchPoints > 0 ||
  navigator.msMaxTouchPoints > 0
;