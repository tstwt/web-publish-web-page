/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-28 14:50:48
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-28 14:50:49
 */
export default (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
