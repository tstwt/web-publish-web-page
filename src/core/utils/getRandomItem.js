/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-28 14:50:33
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-28 14:50:34
 */
import getRandomInt from './getRandomInt';

export default arr => arr[getRandomInt(0, arr.length - 1)];