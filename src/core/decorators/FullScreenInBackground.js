/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-28 14:30:47
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-30 13:20:56
 */
export default (Target) => class FullScreenInBackground extends Target {
    constructor(props) {
        super(window.innerWidth, window.innerHeight, props)

        this.dom.style.position = 'absolute'
        this.dom.style.top = '0'
        this.dom.style.left = '0'
        this.dom.style.zIndex = '-1'
        document.body.appendChild(this.dom)

        if (this.cssRenderer) {
            document.body.appendChild(this.cssDom)
        }

        this.resize = this.resize.bind(this);

        window.addEventListener('resize', this.resize);
        window.addEventListener('orientationchange', this.resize);
        this.resize();
    }

    resize() {
        super.resize(window.innerWidth, window.innerHeight)
    }
}