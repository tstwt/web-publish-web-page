/*
 * @Description: 
 * @Author: Tang Weitian
 * @Date: 2021-09-29 11:01:45
 * @LastEditors: Tang Weitian
 * @LastEditTime: 2021-09-30 17:05:03
 */
import { Object3D, FontLoader } from 'three';
import { CSS3DObject } from 'three-css3drenderer'
import { TimelineLite, Back } from 'gsap';

import fontFile from '../utils/fontFile';

const fontLoader = new FontLoader();
const font = fontLoader.parse(fontFile);

export default class AnimatedButton extends Object3D {
  constructor(name, click, { size = 0.8, width = 30, height = 3, color = '#000000', duration = 0.6, opacity = 1, wireframe = false } = {}) {
    super();

    this.size = size;

    const div = document.createElement('div')
    const button = document.createElement('button')
    button.innerText = name;
    button.style.transform = 'scale(0.05)'
    button.click = click;
    div.appendChild(button)

    const object = new CSS3DObject(div)
    this.add(object)

    // Timeline
    this.tm = new TimelineLite({ paused: true });
    this.tm.set({}, {}, `+=${duration * 1.1}`)
    this.children.forEach((letter) => {
      const data = {
        opacity: 0,
        position: -0.5,
      };
      this.tm.to(data, duration, {
        opacity,
        position: 0,
        ease: Back.easeOut.config(2),
        onUpdate: () => {
          // letter.material.opacity = data.opacity;
          letter.position.y = data.position;
          letter.position.z = data.position * 2;
          letter.rotation.x = data.position * 2;
        }
      }, `-=${duration - 0.03}`);
    });

    // Bind
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
  }

  show() {
    this.tm.play();
  }

  hide() {
    this.tm.reverse();
  }
}